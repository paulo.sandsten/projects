# READ ME

## Install

```bash
pnpm i
```

## Run the dev server

```bash
pnpm run dev
```

## Run the tests

```bash
pnpm run test
```

> Using Mock Service Worker. Meaning, we can replace it with a real backend and expect the same results.
