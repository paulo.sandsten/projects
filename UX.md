## UI/UX questions

- Same color on edit and delete icons. Currently, they are different colors and also different intensity when hover state. Is this intentional? I made it as directed.
- Header has two different shadows. I went wiith the first that is darker when the project list page is empty and lighter when items exist. On page 4, there is no shadow at all.
- Header has two different logos. I went with one.
- Main content has two different backgrounds. Seems that it's white when no items are present. Is this intentional?
- There are over 8 different font colors of black/gray. Is this intentional or can we reduce that number and still achieve the same visual hierarchy?
- Outline on icon buttons for accessibility when focusing?
- 24px left padding on project card, but 32px on the right side. Is this intentional? I followed it.
- What if we have a naming collision?
- What if we are leaving the name empty?
- What is the limit of the name?
- Maybe we can have different image sizes for different screen sizes?
- Cursor in input has two different colors. Is this intentional? I went with one.
- Modal has the padding of 32, 41, 23, 32. I took the decision to make it 32, 40, 24, 32.

## Decisions

- Removing the scrolling bounce and putting scrolling on main content instead to give it a more "app" feel.
- Making the date format local to the locale of the user's browser: the US will have "am and pm".
- Converting some colors to hex.
- Removing assets that are adding to the code bundle or extra http requests.
- Keeping the assets and not using Ant Design icons as delete differs. Don't want to create inconsistancy or more "muddy" code base by using both methods.
- Optimizing the assets and reducing their size.
  1. Edit button 65.63%
  1. Delete button 87.54%
  1. Plus sign 94.21%
  1. Question 59.59%
  1. Thunkable beaver 58%
  1. Default icon 56%

## Developer notes

- Using composition patterns over prop drilling and passing components as "children" where it makes sense.

> Decisions don't reflect strong opinions that can't be changed :)

## Useful articles that explains a bit on dev decisions.

https://epicreact.dev/one-react-mistake-thats-slowing-you-down/
https://kentcdodds.com/blog/testing-implementation-details
