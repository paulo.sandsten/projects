import { Icon } from "../..";
import { IconSize, IconType } from "../../../common";
import styles from "./EditButton.module.css";

interface P {
  handleClick: () => void;
}

export const EditButton: React.FC<P> = ({ handleClick }) => {
  return (
    <button className={styles.button} onClick={handleClick}>
      <Icon size={IconSize.XSMALL} icon={IconType.PEN} />
    </button>
  );
};
