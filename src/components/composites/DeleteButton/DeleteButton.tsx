import { Icon } from "../..";
import { IconSize, IconType } from "../../../common";
import styles from "./DeleteButton.module.css";

interface P {
  handleClick: () => void;
}

export const DeleteButton: React.FC<P> = ({ handleClick }) => {
  return (
    <button className={styles.button} onClick={handleClick}>
      <Icon size={IconSize.XSMALL} icon={IconType.TRASH_CAN} />
    </button>
  );
};
