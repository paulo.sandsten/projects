import { Modal } from "antd";
import { ReactComponent as ExclamationCircleOutlined } from "../../../assets/images/question.svg";
import styles from "./ConfirmModal.module.css";

interface P {
  isOpen: boolean;
  handleConfirm: () => void;
  handleCancel: () => void;
}

export const ConfirmModal: React.FC<P> = ({
  isOpen,
  handleConfirm,
  handleCancel,
}) => (
  <Modal
    className={styles.modal}
    open={isOpen}
    onOk={handleConfirm}
    onCancel={handleCancel}
    okText="Yes"
    cancelText="No"
    width={433}
    closeIcon={false}
  >
    <ExclamationCircleOutlined className={styles.icon} />
    <p className={styles.title}>
      Are you sure you want to delete this project?
    </p>
    <p className={styles.text}>This action can't be undone.</p>
  </Modal>
);
