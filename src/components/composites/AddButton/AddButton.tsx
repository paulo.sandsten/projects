import { FloatButton } from "antd";
import { Icon } from "../../";
import { IconSize, IconType } from "../../../common";
import styles from "./AddButton.module.css";

interface P {
  handleClick: () => void;
}

export const AddButton: React.FC<P> = ({ handleClick }) => {
  return (
    <FloatButton
      className={styles.button}
      icon={<Icon size={IconSize.XSMALL} icon={IconType.PLUS_SIGN} />}
      type="primary"
      onClick={handleClick}
    />
  );
};
