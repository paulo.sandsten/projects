import { useState } from "react";
import { Typography, Input } from "antd";
import { Project, dateTimeFormatter } from "../../../common";
import defaultProjectIcon from "../../../assets/images/defaultProjectIcon_2x.png";
import styles from "./ProjectItem.module.css";
const { Text } = Typography;

export enum ProjectItemState {
  IDLE = "idle",
  NEW = "new",
  EDIT = "edit",
  DELETE = "delete",
}

interface P {
  project: Project;
  state: ProjectItemState;
  handleSave: (value: string, project: Project) => void;
  editButton: React.ReactNode;
  deleteButton: React.ReactNode;
}

const { formatDate, formatTime } = dateTimeFormatter();

export const ProjectItem: React.FC<P> = ({
  project,
  handleSave,
  editButton,
  deleteButton,
}) => {
  const [value, setValue] = useState("");
  const [hoverOnDelete, setHoverOnDelete] = useState(false);

  return (
    <article className={hoverOnDelete ? styles[`item--hover`] : styles.item}>
      <div className={styles.box}>
        <div className={styles.imageWrapper}>
          <img src={project.image ?? defaultProjectIcon} alt="project image" />
        </div>

        {project.state === ProjectItemState.NEW ||
        project.state === ProjectItemState.EDIT ? (
          <Input
            className={styles.input}
            onBlur={() => handleSave(value, project)}
            onKeyUp={(e) => {
              if (e.key === "Enter") {
                handleSave(value, project);
              }
            }}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setValue(e.target.value)
            }
            type="text"
            placeholder="Name your project"
            autoComplete="off"
            autoFocus
          />
        ) : (
          <Text className={styles.name} ellipsis>
            {project.name}
          </Text>
        )}

        {(project.state === ProjectItemState.IDLE ||
          project.state === ProjectItemState.DELETE) && (
          <div className={styles.editButton}>{editButton}</div>
        )}

        {project.state !== ProjectItemState.NEW && (
          <Text className={styles.date}>
            {formatDate(project.created)}
            <span className={styles.time}>{formatTime(project.created)}</span>
          </Text>
        )}

        {project.state !== ProjectItemState.NEW && (
          <div
            className={styles.deleteButton}
            onMouseEnter={() => setHoverOnDelete(true)}
            onMouseLeave={() => setHoverOnDelete(false)}
          >
            {deleteButton}
          </div>
        )}
      </div>
    </article>
  );
};
