import styles from "./PageHeader.module.css";
import { Typography } from "antd";
import logo from "../../../assets/images/ThunkableBeaver.png";

const { Title } = Typography;

interface P {
  heading: string;
  actionButton: React.ReactNode;
}

export const PageHeader: React.FC<P> = ({ heading, actionButton }) => (
  <>
    <div className={styles.box}>
      <div className={styles.imageWrapper}>
        <img src={logo} alt="logo" />
      </div>
      <Title className={styles.heading}>{heading}</Title>
      {actionButton}
    </div>
  </>
);
