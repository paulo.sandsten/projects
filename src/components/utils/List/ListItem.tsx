import { useMotionValue, Reorder } from "framer-motion";
import { useRaisedShadow } from "./use-raised-shadows";

interface P {
  item: string;
  render: (data: any) => React.ReactNode;
}

export const ListItem: React.FC<P> = ({ item, render }) => {
  const y = useMotionValue(0);
  const boxShadow = useRaisedShadow(y);

  return (
    <Reorder.Item value={item} id={item} style={{ boxShadow, y }}>
      <span>{render(item)}</span>
    </Reorder.Item>
  );
};
