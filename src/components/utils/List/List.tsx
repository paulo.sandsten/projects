import { Dispatch } from "react";
import { Reorder } from "framer-motion";
import { ListItem } from "./ListItem";
import styles from "./List.module.css";

type ListItem = {
  [key: string]: string;
  id: string;
};

interface P {
  list: ListItem[] | any[];
  render: (data: any) => React.ReactNode;
  handleSort: Dispatch<any>;
}

export const List: React.FC<P> = ({ list, render, handleSort }) => (
  <Reorder.Group
    className={styles.list}
    axis="y"
    onReorder={handleSort}
    values={list}
  >
    {list.map((item: any) => (
      <ListItem key={item.id} item={item} render={render} />
    ))}
  </Reorder.Group>
);
