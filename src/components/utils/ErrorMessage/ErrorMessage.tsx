import styles from "./ErrorMessage.module.css";

interface P {
  message: string;
}

export const ErrorMessage: React.FC<P> = ({ message }) => (
  <div className={styles.box}>{message}</div>
);
