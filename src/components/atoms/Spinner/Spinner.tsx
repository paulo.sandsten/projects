import { Spin } from "antd";
import styles from "./Spinner.module.css";

export const Spinner = () => (
  <div className={styles.box}>
    <Spin />
  </div>
);
