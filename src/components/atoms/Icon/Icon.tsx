import React from "react";
import { classnames, IconSize, IconType } from "../../../common";
import { ReactComponent as PlusSign } from "../../../assets/images/plus-sign.svg";
import { ReactComponent as Pen } from "../../../assets/images/edit-icon.svg";
import { ReactComponent as TrashCan } from "../../../assets/images/delete-icon.svg";
import { ReactComponent as Question } from "../../../assets/images/question.svg";

import styles from "./Icon.module.css";

interface IconTypesIndex {
  [key: string]: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
}

interface IconProps {
  icon: IconType;
  size: IconSize;
  onClick?: () => void;
}

const IconTypes: IconTypesIndex = {
  plusSign: PlusSign,
  pen: Pen,
  trashCan: TrashCan,
  question: Question,
};

const Icon = ({ icon, size = IconSize.MEDIUM, onClick }: IconProps) => {
  const Component = IconTypes[icon];

  const classes = classnames(styles.base, {
    [`${styles[size]}`]: !!size,
  });

  return (
    <span className={classes} onClick={onClick}>
      <Component />
    </span>
  );
};

export { Icon };
