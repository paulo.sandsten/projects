import { ConfigProvider } from "antd";
import styles from "./AppLayout.module.css";

interface P {
  hasContent?: boolean;
  headerContent: React.ReactNode;
  mainContent: React.ReactNode;
}

export const AppLayout: React.FC<P> = ({
  hasContent,
  headerContent,
  mainContent,
}) => {
  return (
    <ConfigProvider
      theme={{
        token: {
          fontFamily: `"SourceSans", -apple-system, BlinkMacSystemFont, "Segoe UI",
        Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif,
        "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"`,
        },
        components: {
          FloatButton: {
            colorPrimary: "#4a475f",
            colorPrimaryHover: "#3d3a4f",
          },
        },
      }}
    >
      <header className={hasContent ? styles.header : styles[`header--empty`]}>
        {headerContent}
      </header>
      <main className={hasContent ? styles.content : styles[`content--empty`]}>
        {mainContent}
      </main>
    </ConfigProvider>
  );
};
