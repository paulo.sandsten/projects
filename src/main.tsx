import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "./services/state/store.ts";
import { App } from "./App.tsx";
import "./common/styles/fonts.css";
import "./common/styles/variables.css";
import "./common/styles/globals.css";

if (process.env.NODE_ENV === "development") {
  const { worker } = await import("./services/mockApi/browser.ts");
  worker.start();
}

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);
