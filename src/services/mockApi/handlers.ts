import { rest } from "msw";
import { mockedProjects } from "./fixtures/projects";
import { Project } from "../../common";

const DELAY = 0;

let inMemory = [...mockedProjects];

export const handlers = [
  rest.get("/api/projects", (_req, res, ctx) => {
    /**
     * In an ideal frontend to backend contract, we should be able to get projects
     * sorted, filtered, etc from the query strings.
     *
     * Now, we are cutting corners here as this is just a small showcase project, so
     * we are always getting the projects sorted by their "priority" order.
     *
     * This order is set by the user when drag and drop are set.
     */
    const sortedProjectsByOrder = inMemory.sort((a, b) => a.order - b.order);

    return res(
      ctx.delay(DELAY),
      ctx.status(200),
      ctx.json(sortedProjectsByOrder)
    );
  }),

  rest.post("/api/projects", (req, res, ctx) =>
    req.json().then((payload) => {
      inMemory = [payload, ...inMemory];
      return res(ctx.delay(DELAY), ctx.status(200), ctx.json(payload));
    })
  ),

  rest.patch("/api/projects", (req, res, ctx) => {
    return req.json().then((payload) => {
      const map = new Map();
      payload.forEach((project: Project) => {
        map.set(project.id, project.order);
      });

      inMemory = inMemory.map((p) =>
        map.has(p.id)
          ? {
              ...p,
              order: map.get(p.id),
            }
          : p
      );

      return res(ctx.delay(DELAY), ctx.status(200), ctx.json(payload));
    });
  }),

  rest.patch("/api/projects/:id", (req, res, ctx) => {
    const { id } = req.params;

    return req.json().then((payload) => {
      inMemory = inMemory.map((p) =>
        id === p.id
          ? {
              ...p,
              ...payload,
            }
          : p
      );
      return res(ctx.delay(DELAY), ctx.status(200), ctx.json(payload));
    });
  }),

  rest.delete("/api/projects/:id", (req, res, ctx) => {
    const { id } = req.params;
    inMemory = inMemory.filter((project: Project) => !(id === project.id));
    return res(ctx.delay(DELAY), ctx.status(200), ctx.json(inMemory));
  }),
];
