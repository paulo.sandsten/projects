import { ProjectItemState } from "../../../common";

const PROJECT_ITEMS = 0;

export const mockedProjects = [...Array(PROJECT_ITEMS)].map((_, i) => ({
  id: i.toString(),
  name: `My project ${i}`,
  created: "2020-05-14T04:00:00Z",
  state: ProjectItemState.IDLE,
  order: i,
}));
