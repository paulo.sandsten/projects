import { ProjectItemState } from "..";

export type Project = {
  id: string;
  name: string;
  created: string;
  state: ProjectItemState;
  order: number;
  image?: string;
};
