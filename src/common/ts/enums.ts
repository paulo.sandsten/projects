export enum IconSize {
  XSMALL = "x-small",
  SMALL = "small",
  MEDIUM = "medium",
}

export enum IconType {
  PLUS_SIGN = "plusSign",
  PEN = "pen",
  TRASH_CAN = "trashCan",
  QUESTION = "question",
}

export enum ProjectItemState {
  IDLE = "idle",
  NEW = "new",
  EDIT = "edit",
  DELETE = "delete",
}
