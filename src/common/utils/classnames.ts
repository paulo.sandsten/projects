// @ts-ignore
const checkObject = (obj) => {
  return Object.keys(obj).filter((o) => !!obj[o]);
};

// classnames("foo", ["boo", "zoo"], { maybe: true, no: false});
// will return: "foo boo zoo maybe"
export const classnames = (
  ...rest: (string | { [x: string]: boolean })[]
): string => {
  return rest
    .flatMap((c) => {
      if (typeof c === "object" && !Array.isArray(c)) {
        return checkObject(c);
      } else {
        return c;
      }
    })
    .join(" ")
    .trim();
};
