export const dateTimeFormatter = () => {
  const formatDate = (dateString: string) => {
    const options = { year: "numeric", month: "long", day: "numeric" };

    // @ts-ignore
    return new Date(dateString).toLocaleDateString(undefined, options);
  };

  const formatTime = (dateString: string) => {
    const options = { hour: "numeric", minute: "numeric" };

    // @ts-ignore
    return new Date(dateString).toLocaleTimeString(undefined, options);
  };

  return {
    formatDate,
    formatTime,
  };
};
