import { dateTimeFormatter } from "..";

describe("dateTimeFormatter", () => {
  it("Will return locale date from an ISO date time", () => {
    const { formatDate } = dateTimeFormatter();

    const input = "2020-05-14T04:00:00Z";
    const expectedOutput = "May 13, 2020"; // This is US only test.

    expect(formatDate(input)).toBe(expectedOutput);
  });

  it("Will return locale time from an ISO date time", () => {
    const { formatTime } = dateTimeFormatter();

    const input = "2020-05-14T04:00:00Z";
    const expectedOutput = "9:00 PM"; // This is US only test.

    expect(formatTime(input)).toBe(expectedOutput);
  });
});
