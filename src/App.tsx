import { useEffect, useState } from "react";
import { Project, ProjectItemState } from "./common";
import {
  useAddNewProjectMutation,
  useDeleteProjectMutation,
  useEditProjectMutation,
  useEditProjectsMutation,
  useGetProjectsQuery,
} from "./services/state/apiSlice";
import {
  AddButton,
  AppLayout,
  ConfirmModal,
  DeleteButton,
  ErrorMessage,
  EditButton,
  List,
  PageHeader,
  ProjectItem,
  Spinner,
} from "./components";

export const App = () => {
  const {
    data: projects,
    isLoading,
    isSuccess,
    isError,
    error,
  } = useGetProjectsQuery(undefined);
  const [editProjects, { isLoading: isEditProjectsMutationLoading }] =
    useEditProjectsMutation();
  const [addNewProject, { isLoading: isAddMutationLoading }] =
    useAddNewProjectMutation();
  const [deleteProject, { isLoading: isDeleteMutationLoading }] =
    useDeleteProjectMutation();
  const [editProject, { isLoading: isEditMutationLoading }] =
    useEditProjectMutation();
  const [sortedProjects, setSortedProjects] = useState(projects);
  const [newProject, setNewProject] = useState<Project>();
  const [confirmModalIsOpen, setConfirmModalIsOpen] = useState(false);
  const [_, setDeleteProjectBucket] = useState<Project | null>(null);

  /**
   * When we are creating a new project, we need to make sure we are updating the state
   * with the new project item on the top. As we we are using RTK, we really don't need
   * to also use the "normal" redux store for this project due to it's size. We use
   * instead React's local state with hooks to update the view until we are making the
   * changes that needs saving/storing: save, edit, delete.
   *
   * Note that we are only giving it temp values for the `id` and `created` props.
   * These values should technically come from the BE once we get a response back.
   */
  const handleNewProject = () => {
    const tempId = Date.now().toString();
    const tempCreatedDate = new Date(Date.now()).toISOString();
    setNewProject({
      id: tempId,
      name: "",
      created: tempCreatedDate,
      state: ProjectItemState.NEW,
      order: -1,
    });
  };

  /**
   * Adding a bit of logic here that we don't want the user to be able to save the new
   * project if there is no value in the input.
   */
  const handleSave = async (value: string, project: Project) => {
    if (!isAddMutationLoading && value) {
      const payload = {
        ...project,
        name: value,
        state: ProjectItemState.IDLE,
      };

      try {
        project.state === ProjectItemState.NEW
          ? await addNewProject(payload)
          : await editProject(payload);
      } catch (err) {
        console.error("Failed to save the post: ", err);
      }
    }
  };

  const handleEditState = (project: Project) => {
    if (!isEditMutationLoading) {
      setSortedProjects((projects: Project[]) =>
        projects.map((p: Project) => {
          if (project.id === p.id) {
            return {
              ...project,
              state: ProjectItemState.EDIT,
            };
          } else {
            return p;
          }
        })
      );
    }
  };

  const handleDelete = (project: Project) => {
    if (!isDeleteMutationLoading) deleteProject(project);
  };

  const handleConfirmModal = (project: Project) => {
    setConfirmModalIsOpen(() => {
      setDeleteProjectBucket(project);
      return true;
    });
  };

  const handleConfirm = (confirm: boolean) => {
    setDeleteProjectBucket((project) => {
      if (confirm) {
        handleDelete(project as Project);
      }
      setConfirmModalIsOpen(false);
      return null;
    });
  };

  const handleSort = (projects: Project[]) => {
    const updatedSortingOrderOnProjects = projects.map((p, i) => ({
      ...p,
      order: i,
    }));
    if (!isEditProjectsMutationLoading) {
      // Greedy! Needs throttling techniques.
      editProjects(updatedSortingOrderOnProjects);
    }
    setSortedProjects(updatedSortingOrderOnProjects);
  };

  useEffect(() => {
    setSortedProjects(() => {
      setNewProject(undefined);
      return projects;
    });
  }, [projects]);

  return (
    <>
      <AppLayout
        hasContent={!!sortedProjects?.length || !!newProject}
        headerContent={
          <PageHeader
            heading={"MY PROJECTS"}
            actionButton={<AddButton handleClick={handleNewProject} />}
          />
        }
        mainContent={
          <>
            {isLoading && <Spinner />}
            {isError && <ErrorMessage message={error.toString()} />}
            {isSuccess && sortedProjects && (
              <List
                handleSort={handleSort}
                list={
                  newProject
                    ? [{ ...newProject }, ...sortedProjects]
                    : sortedProjects
                }
                render={(project) => (
                  <ProjectItem
                    project={project}
                    handleSave={handleSave}
                    state={ProjectItemState.EDIT}
                    editButton={
                      <EditButton
                        handleClick={() => handleEditState(project)}
                      />
                    }
                    deleteButton={
                      <DeleteButton
                        handleClick={() => handleConfirmModal(project)}
                      />
                    }
                  />
                )}
              />
            )}
          </>
        }
      />
      <ConfirmModal
        handleConfirm={() => handleConfirm(true)}
        handleCancel={() => handleConfirm(false)}
        isOpen={confirmModalIsOpen}
      />
    </>
  );
};
